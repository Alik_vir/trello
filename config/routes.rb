Rails.application.routes.draw do
  resources :todos
  resources :projects

  root 'projects#index'
end
